
module "SG" {
  source             = "../module/sg"
  name_prefix        = "${var.name_prefix}"
  RANGE_SG_IPS       = "${var.RANGE_SG_IPS}"
  ENV                = "${var.ENV}"
  requerente         = "${var.requerente}"
  vpc_id             = "${var.vpc_id}"
  services_ports     = "${var.services_ports}"
  IPS_VPC_ACCESS_EKS = "${var.IPS_VPC_ACCESS_EKS}"

}


data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}


module "eks" {
  source                               = "../module/k8s"
  cluster_name                         = "${var.cluster-name}-${var.ENV}"
  subnets                              = "${var.subnets_ids}"
  vpc_id                               = "${var.vpc_id}"
  map_roles                            = "${var.map_roles}"
  manage_aws_auth                      = true
  worker_additional_security_group_ids = ["${module.SG.sgoutput}"]
  cluster_enabled_log_types            = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  key_name                             = "${var.keyName}"
  cluster_version                      = "${var.versionCluster}"
  cluster_endpoint_public_access_cidrs = ["0.0.0.0/0"]
  worker_groups = [
    {
      instance_type                 = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.AWS_TYPE_INSTANCE}%{else}t2.medium%{endif}"
      asg_max_size                  = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.DMAXEKS}%{else}3%{endif}"
      asg_desired_capacity          = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.DMIN}%{else}2%{endif}"
      kubelet_extra_args            = "--node-labels=app=microservices"
      additional_security_group_ids = ["${module.SG.sgoutput}"]
    }
  ]
  tags = {
    Name                                                   = "${var.cluster-name}-${var.ENV}"
    terraform                                              = true
    app                                                    = "NOME DO APP" 
    projeto                                                = "NOME DO PROJETO"
    requerente                                             = "${var.requerente}"
    ambiente                                               = "${var.ENV}"
    "kubernetes.io/cluster/${var.cluster-name}-${var.ENV}" = "shared" #TAG OBRIGATÓRIA NA VPC, ASSIM O EKS CONSEGUE CRIAR OS ELB PARA EXPOR O SERVIÇO
    modalidade                                             = "projeto"


  }
}


module CLUSTER-RDS {
  source                          = "../module/cluster-rds/"
  master_username_cluster         = "${var.master_username_cluster}"
  master_password_cluster         = "${var.master_password_cluster}"
  backup_retention_period_cluster = "${var.backup_retention_period_cluster}"
  preferred_backup_window_cluster = "${var.preferred_backup_window_cluster}"
  name                            = "${var.NAME_CLUSTER_RDS}${var.ENV}"
  ENV                             = "${var.ENV}"
  requerente                      = "${var.requerente}"
  COUNT                           = "${var.COUNT}"
  subnet_ids                      = "${var.subnet_ids}"
  RANGE_IPS_SR                    = "${var.RANGE_IPS_SR}"
  vpc_id                          = "${var.vpc_id}"
  port                            = "${var.PORT_CLUSTER}"
}