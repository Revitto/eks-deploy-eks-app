variable "AWS_REGION" {
  type    = "string"
  default = "us-west-2"
}

variable "vpc_id" {
  type    = "string"
  default = "seu-vpc-id"
}

variable "requerente" {
  type    = "string"
  default = "requerente"

}

variable "map_public_true" {
  type    = "string"
  default = "true"
}

variable "ENV" {
  type    = "string"
  default = "develop"

}

#####################################EKS############################
variable "RANGE_SG_IPS" {
  type    = list
  default = [""]
  description = "Lista de range para acesso ao eks para o db"
}
variable "name_prefix" {
  type    = string
  default = ""
  description = "Nome para eks"
}

variable "cluster-name" {
  default = ""
  type    = string
  description = "Nome do produto "
}

variable subnets_ids {
  type    = list
  default = [ ]
  description = "Lista de subnets ids privadas para nodes eks" 
}

variable "keyName" {
  type        = "string"
  default     = ""
  description = "Nome da chave de conta para acesso aos nós se preciso crise analise física"
}

variable "versionCluster" {
  type    = "string"
  default = "1.16"
  description = "Versão do cluster eks"
}

variable services_ports {
  type = list
  default = [
    "22"
  ]
  description = "lista de portas acesso para gerenciamento ao kubernetes"
}


variable "map_roles" {
  description = "AdditionalIAMrolestoaddtotheaws-authconfigmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::conta-aws:role/EKS_AssumeRole"
      username = "EKS_AssumeRole"
      groups   = ["system:masters"]
    },
  ]
  description = "Assume roles entre contas, para administração do cluster "
}



variable "AWS_TYPE_INSTANCE" {
  type        = string
  default     = "t2.large"
  description = "tipo da instancia"
}

variable "DEKS" {
  type    = string
  default = "4"
  description = "Desejavel eks ec2"


}

variable "DMAXEKS" {
  type    = string
  default = "8"
  description = "Maximo eks ec2"

}

variable "DMIN" {
  type    = string
  default = "4"
  description = "minimo eks ec2"
}


################CLUSTER RDS###################

variable cluster_identifier {
  type        = string
  default     = ""
  description = "Nome do cluster"
}


variable master_username_cluster {
  type        = string
  default     = ""
  description = "usuário master do cluster"
}

variable master_password_cluster {
  type        = string
  default     = ""
  description = "password master do cluster"
}

variable backup_retention_period_cluster {
  type        = string
  default     = "3"
  description = "Numero de quantidades de retenção de backup"
}

variable preferred_backup_window_cluster {
  type        = string
  default     = "07:00-09:00"
  description = "Janela para início do backup"
}

variable NAME_CLUSTER_RDS {
  type        = string
  default     = ""
  description = "Nome de exemplo do cluster"
}

variable AWS_INSTANCE_CLASS_RDS {
  type        = string
  default     = "db.t3.2xlarge"
  description = "Tipo de Instancia RDS"
}

variable COUNT {
  type        = string
  default     = "3"
  description = "número de instancias"
}

variable PORT_CLUSTER {
  type        = string
  default     = "3306"
  description = "description"
}

variable RANGE_IPS_SR {
  type    = list
  default = [""]
   description = "lISTA DE RANGE PARA ACESSOS AO CLUSTER RDS"
}
variable IPS_VPC_ACCESS_EKS {
  type = list
  default = [""]
  description = "Lista de ips para acesso ao eks, range da vpc para admisntracao ou range de vpn para a mesma!!!!!!"

}

variable "subnet_ids"{
  type =  list
  default = [""]
  description = "lista de subnets para o cluster RDs OBRIGATÓRIOS DUAS SUBNETS EM AZS DISTINTAS"
}