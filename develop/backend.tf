terraform {
  backend "s3" {
    bucket         = "bucket/"
    dynamodb_table = "table-dynamo"
    key            = "dir/object"
    region         = "us-west-2"
    encrypt        = true
  }
}