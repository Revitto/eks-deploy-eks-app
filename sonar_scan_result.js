const request = require('request');

function result() {
    const url = `${process.env.URL_SONAR}/api/qualitygates/project_status?projectKey=${process.env.PROJECT_SONAR}`;
    console.log('URL_SONAR_RESULT: ', url);

    const options = {
        url: url,
        method: 'GET',
        json: true,
        auth: { user: process.env.TOKEN_SONAR }
    }

    request(options, function (error, _response, body) {
        console.log(body);
        if (body.projectStatus.status === 'ERROR') {
            throw Error('ERROR Stataus Sonar');
        }
        if (error) console.log(error);
    });
}

result();