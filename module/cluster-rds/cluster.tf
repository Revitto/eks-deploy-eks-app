resource "aws_db_subnet_group" "app-subnet" {
  name        = "${var.name}${var.ENV}"
  description = "RDS cluster group"
  subnet_ids  = "${var.subnet_ids}"
}

resource "aws_rds_cluster" "clusterapp" {
  db_subnet_group_name    = "${aws_db_subnet_group.app-subnet.name}"
  cluster_identifier      = "${var.name}${var.ENV}"
  database_name           = "${var.name}${var.ENV}"
  master_username         = "${var.master_username_cluster}"
  master_password         = "${var.master_password_cluster}"
  backup_retention_period = "${var.backup_retention_period_cluster}"
  preferred_backup_window = "${var.preferred_backup_window_cluster}"
  skip_final_snapshot     = true # skip final snapshot when doing terraform destroy

  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "projeto"
    Projeto                          = "projeto"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/RDS-app" = "${var.ENV}"
  }
}

resource "aws_rds_cluster_instance" "app1" {
  apply_immediately  = true
  cluster_identifier = "${aws_rds_cluster.clusterapp.id}"
  identifier         = "${var.name}-${var.ENV}-1"
  instance_class     = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.AWS_INSTANCE_CLASS_RDS}%{else}db.r4.large%{endif}"

  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "app"
    Projeto                          = "app"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/RDS-app" = "shared"
  }
}

resource "aws_rds_cluster_instance" "app2" {
  apply_immediately  = true
  cluster_identifier = "${aws_rds_cluster.clusterapp.id}"
  identifier         = "${var.name}-${var.ENV}-2"
  instance_class     = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.AWS_INSTANCE_CLASS_RDS}%{else}db.r4.large%{endif}"

  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "app"
    Projeto                          = "app"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/RDS-app" = "shared"
  }
}

resource "aws_rds_cluster_instance" "app3" {
  apply_immediately  = true
  cluster_identifier = "${aws_rds_cluster.clusterapp.id}"
  identifier         = "${var.name}-${var.ENV}-3"
  instance_class     = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.AWS_INSTANCE_CLASS_RDS}%{else}db.r4.large%{endif}"
  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "app"
    Projeto                          = "app"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/RDS-app" = "shared"
  }
}

resource "aws_rds_cluster_endpoint" "eligible" {
  cluster_identifier          = "${aws_rds_cluster.clusterapp.id}"
  cluster_endpoint_identifier = "reader"
  custom_endpoint_type        = "READER"

  excluded_members = [
    "${aws_rds_cluster_instance.app1.id}",
    "${aws_rds_cluster_instance.app2.id}",
  ]
  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "app"
    Projeto                          = "app"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/RDS-app" = "shared"
  }
}

resource "aws_rds_cluster_endpoint" "static" {
  cluster_identifier          = "${aws_rds_cluster.clusterapp.id}"
  cluster_endpoint_identifier = "static"
  custom_endpoint_type        = "READER"

  static_members = [
    "${aws_rds_cluster_instance.app1.id}",
    "${aws_rds_cluster_instance.app3.id}",
  ]
  tags = {
    Name                             = "${var.name}-cluster${var.ENV}"
    Terraform                        = "true"
    Ambiente                         = "${var.ENV}"
    APP                              = "app"
    Projeto                          = "app"
    Requerente                       = "${var.requerente}"
    "kubernetes.io/cluster/RDS-app" = "shared"
  }
}