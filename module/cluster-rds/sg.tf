resource "aws_security_group" "sg-cluster" {
  vpc_id      = "${var.vpc_id}"
  name        = "${var.name}-${var.ENV}"
  description = "security group that allows ssh and all egress traffic"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.RANGE_IPS_SR}"
  }
  tags = {
    Name = "sg-cluster"
  }
}

resource "aws_security_group" "allow-app" {
  vpc_id      = "${var.vpc_id}"
  name        = "${var.name}-doc-${var.ENV}"
  description = "${var.name}-${var.ENV}"
  ingress {
    from_port       = "${var.port}"
    to_port         = "${var.port}"
    protocol        = "tcp"
    security_groups = ["${aws_security_group.sg-cluster.id}"] # allowing access from our example instance
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }
  tags = {
    Name = "allow-app"
  }
}

