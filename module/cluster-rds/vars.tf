variable master_username_cluster {}
variable master_password_cluster {}
variable backup_retention_period_cluster {}
variable preferred_backup_window_cluster {}
variable name {}
variable ENV {}
variable requerente {}

variable AWS_INSTANCE_CLASS_RDS {
  type        = string
  default     = "db.r4.2xlarge"
  description = "Tipo de instancia do document db"
}
variable COUNT {}
variable subnet_ids {}
variable RANGE_IPS_SR {}
variable vpc_id {}
variable port {}
